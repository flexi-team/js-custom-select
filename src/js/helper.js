'use strict';

if (!Element.prototype.remove) {
    Element.prototype.remove = () => {
        if (this.parentNode) {
            this.parentNode.removeChild(this);
        }
    };
}

(function (e) {
    e.closest = function (css) {
        var node = this;

        while (node) {
            if (node.matches(css))
                return node;
            else
                node = node.parentElement;
        }
        return null;
    };
})(Element.prototype);

if (!Element.prototype.matches) {
    Element.prototype.matches =
            Element.prototype.matchesSelector ||
            Element.prototype.webkitMatchesSelector ||
            Element.prototype.mozMatchesSelector ||
            Element.prototype.msMatchesSelector;
}

if (!Object.assign) {
    Object.defineProperty(Object, 'assign', {
        enumerable: false,
        configurable: true,
        writable: true,
        value: function (target) {
            'use strict';
            if (target === undefined || target === null) {
                throw new TypeError('Cannot convert first argument to object');
            }
            var to = Object(target);
            for (var i = 1; i < arguments.length; i++) {
                var nextSource = arguments[i];
                if (nextSource === undefined || nextSource === null) {
                    continue;
                }
                var keysArray = Object.keys(Object(nextSource));
                for (var nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
                    var nextKey = keysArray[nextIndex];
                    var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
                    if (desc !== undefined && desc.enumerable) {
                        to[nextKey] = nextSource[nextKey];
                    }
                }
            }
            return to;
        }
    });
}

export const browser = {
    addEventListener: !!window.addEventListener,
    touch: ('ontouchstart' in window) || window.DocumentTouch && document instanceof window.DocumentTouch
};


export const createTag = (tag, attrs) => {

    let _tag = document.createElement(tag);

    if (attrs instanceof Object && Object.keys(attrs).length) {
        for (let key in attrs) {
            _tag.setAttribute(key, attrs[key]);
        }
    }

    return _tag;
};

