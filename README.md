# README #

## Description: ##

*You can review the example here: - [Demo](http://js-custom-select.flexi.ink). This custom select works on all browsers (ie10+) and mobile devices. It was written on pure js, uses es6 standards. It supports dynamic loading, and internal search.*

-----------------------------------------------------------

## You can install it from npm:

```
npm install js-custom-select --save
```

or download from git
```
git clone https://bitbucket.org/flexi-team/js-custom-select.git
```

##To use all capabilities of custom-select, you need include [js-custom-scroll](https://www.npmjs.com/package/js-custom-scroll)

```
npm install js-custom-scroll --save
```

or download from git
```
git clone https://bitbucket.org/flexi-team/js-custom-scroll.git
```

-----------------------------------------------------------

## Example of use:

Include `js-custom-select.css` in the head tag your HTML document

```html
<link rel="stylesheet" href="node_modules/dist/js-custom-select.css"/>
```

Include `js-custom-select.min.js` in the end of your HTML document

```html
<script async src="node_modules/dist/js-custom-select.min.js"></script>
```

### Initialize via javascript:

```JavaScript
<script type="text/javascript">
    var i = 0;
    var select3 = new jsCustomSelect(document.getElementById('test3'), {
        search: true,
        //For dynamic loading you just need to activate js-custom-scroll, and configure `at_Fn` function
        scroll: {
            at: 80,
            at_Fn: function (scrollbar, el) {
                i++;
                var _select = select3.getSelect();
                for (var k = 0; k < 15; k++) {
                    var _opt = document.createElement('option');
                    var data = Math.random();
                    _opt.value = data;
                    _opt.innerHTML = data;
                    _select.appendChild(_opt);
                }
                select3.update();
                return i < 5;
            }
        }
    });
</script>
```

### Initialize via jQuery:

```JavaScript
<script type="text/javascript">
    $('#test').jsCustomSelect({});
</script>
```

### Properties:

* initClass {string} - Sets class for custom select of the block. Defaults to `custom-select-block`.
* max-height {Number} - Sets the restriction for drop-down block height.
* search {boolean} - The ability to search inside the select. Defaults to `false`.
* searchNoResults {string} - The phrase that will be displayed when no results found. Defaults to `No results match`.
* searchInputHeight {Number} - Sets the search block height. Defaults to `36`px.
* placeholder {string} - Placeholder is displayed when no value was selected.
* change: {function} - Function is triggered when element was selected from the custom select.


## Js-Custom-Select API:

Js-Custom-Select exposes a few functions that can be useful for script control of your the custom select.

* `getSelect()` - Get the element select, which was converted into a custom select.
* `selt()` - Get the custom select.
* `getSelected()` -Removes the selected element.
* `update()` - Forcibly rebuilds the custom select.
* `kill() ` - Completely removes the custom select.

## License

Copyright (c) 2017 Aleksejj Shmyrko Licensed under the The MIT License (MIT).